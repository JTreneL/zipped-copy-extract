import os
import shutil
import gzip
import configparser
import sys


def copy_file(zipped,unzipped):
    for files in os.listdir(zipped):
        if files.endswith('.gz'):
            print(f"Copying {files}")
            shutil.move(os.path.join(zipped,files), os.path.join(unzipped,files))
        

def gz_extract(directory):
    os.chdir(directory)
    for item in os.listdir(directory): 
      if item.endswith('.gz'):
        gz_name = os.path.abspath(item) 
        file_name = os.path.basename(gz_name).rsplit('.',1)[0]
        with gzip.open(gz_name,"rb") as f_in, open(file_name,"wb") as f_out:
            shutil.copyfileobj(f_in, f_out)
        #os.remove(gz_name) 


def main():
    config = configparser.ConfigParser()		
    config.read("config.ini")
    targetdir = config['targetdir']
    finaldir = config['finaldir']
    zipped = targetdir["zipped"]
    unzipped = finaldir["unzipped"]
    copy_file(zipped,unzipped)
    gz_extract(unzipped)


if __name__ == "__main__":
    sys.exit(main())
